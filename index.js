const express = require("express");
const cors = require("cors");

const port = process.env.PORT || 8000;
const app = express();

app.use(cors());
app.use(function (request, response, next) {
    const authorization = request.headers["authorization"];
    const regexResult = /basic (.*)/i.exec(authorization);

    if (!regexResult) {
        const realm = "Global";

        response.setHeader("WWW-Authenticate", `Basic realm="${realm}"`);
        return response.sendStatus(401);
    }

    const buffer = Buffer.from(regexResult[1], "base64");
    const credentials = buffer.toString();
    const [username, password] = credentials.split(":");

    if (username !== "user" || password !== "pass") {
        return response.sendStatus(403);
    }

    next();
});

app.get("", function (request, response) {
    response.sendStatus(200);
});

app.listen(port, function () {
    console.log("Listening on port " + port);
});